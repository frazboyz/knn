public class KNNTest {

	/**
	 * ------------------------------------OUTPUT---------------------------------------------
	 * Classified the instance feature to category 1
	 * the nearest k(2) neighbours {
	 * 	 [1.0, 1.0, 10.0, 10.0] - 1/4
	 *	 [2.0, 2.0, 8.0, 6.0] - 1/8
	 * }
	 * ------------------------------------OUTPUT---------------------------------------------
	 */
	
	public static void main(final String[] args) {
		final Feature<Integer>[] features = new Feature[]{
			new Feature<Integer>(1, new double[]{1, 1, 10, 10}),
			new Feature<Integer>(1, new double[]{2, 2, 8, 6}),
			new Feature<Integer>(2, new double[]{51, 23, 32, 23}),
			new Feature<Integer>(2, new double[]{50, 22, 40, 26}),
			new Feature<Integer>(3, new double[]{14, 18, 20, 12}),
			new Feature<Integer>(4, new double[]{11, 9, 18, 12})
		};
		final Feature<Integer> instance = new Feature(new double[]{3, 7, 9, 7});
		final FeatureSpace<Integer> space = new FeatureSpace(2, features);
		space.setInstance(instance);
		instance.category = space.compute();
		System.out.println("Classified the instance feature to category " + instance.category);
		System.out.println("The nearest k(" + space.k + ") neighbours {");
		for (int i = 0; i < space.k; i++) {
			System.out.println("	" + space.features[i].toString());
		}
		System.out.println("}");
	}
	
}