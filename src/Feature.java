import java.util.Arrays;
import java.lang.Math;

/**
 * Created by francis on 28/02/14.
 * @param <T> is the datatype of category/class
 */
public class Feature<T> implements Comparable<Feature<T>> {

    /**
     * The class/category instance that this Feature<T> represents
     */
    public T category;

    /**
     * The distance to the Feature<T> instance
     */
    public int distance;

    /**
     * The attributes of this Feature<T>
     */
    public double[] attributes;

    public Feature(final double[] attributes) {
	this.attributes = attributes;
    }

    public Feature(final T category, final double[] attributes) {
	this(attributes);
	this.category = category;
    }

    @Override
    public int compareTo(final Feature<T> o) {
        return Integer.compare(Math.abs(distance), Math.abs(o.distance));
    }

    @Override
    public String toString() {
        return Arrays.toString(attributes) + " - " + category + "/" + distance;
    }
}