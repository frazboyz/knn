import java.util.*;

/**
 * Created by francis on 28/02/14.
 * @param <T> is the datatype of category/class
 */
public class FeatureSpace<T> {

    /**
     * The amount of nearest neighbors
     */
    public int k = -1;

    /**
     * The instance Feature<T> we are trying to classify
     */
    public Feature<T> instance;

    /**
     * The features/data contained within the FeatureSpace<T>
     */
    public final Feature<T>[] features;

    public FeatureSpace(final Feature<T>[] features) {
    	this.features = features;
    }

    public FeatureSpace(final int k, final Feature<T>[] features) {
	   this.k = k;
	   this.features = features;
    }

    /**
     * Set the instance Feature
     */
    public void setInstance(final Feature<T> instance) {
	   this.instance = instance;
    }

    /**
     * Calculate and return the instance's category/class
     */
    public T compute() {
        /**
         * Calculate the distances between the instance and data
         */
    	for (int d_index = 0; d_index < features.length; d_index++) {
            for (int a_index = 0; a_index < instance.attributes.length; a_index++) {
                features[d_index].distance += instance.attributes[a_index] - features[d_index].attributes[a_index];
            }
        }

        /**
         * Sort the data based of the distance to the instance
         */
        Arrays.sort(features);

        /**
         * If there are less features then k then set k to the features length
         */
        if (features.length < k) {
        	k = features.length;
        }

        /**
         * If k is unset then we will set k to (features.length ^ 0.5)
         */
        if (k == -1) {
        	k = ((Long) Math.round(Math.pow(features.length, 0.5))).intValue();
        }

        /**
         * Create nearest neighbours array with the size of k
         */
        final Feature<T>[] neighbors = new Feature[k];

        /**
         * Add the k nearest neighbours to the array
         */
        for (int i = 0; i < k; i++) {
        	neighbors[i] = features[i];
        }
        return getMajority(neighbors);
    }

    /**
     * Created to get the mose common <T> neighbor
     *
     * @param neighbors an array of MetricInstance<T> which are the k nearest neighbors
     * @return the most common <T> neighbor
     */
    public T getMajority(final Feature<T>[] neighbors) {
        if (neighbors.length == 2) return (neighbors[0].distance < neighbors[1].distance ? neighbors[0].category : neighbors[1].category);
        if (neighbors.length == 1) return neighbors[0].category;
        final Map<T, Integer> occurrence = new HashMap<>();
        for (final Feature<T> feature : neighbors) {
            if (occurrence.containsKey(feature.category)) {
                occurrence.put(feature.category, occurrence.get(feature.category) + 1);
            } else {
                occurrence.put(feature.category, 1);
            }
        }
        Map.Entry<T, Integer> majority = null;
        for (final Map.Entry<T, Integer> entry : occurrence.entrySet()) {
            if (majority == null || majority.getValue() < entry.getValue()) {
                majority = entry;
            }
        }
        return majority == null ? null : majority.getKey();
    }

}